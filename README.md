# README #

This README file contains step by step guidance of  how to encode videos using ffmpeg encoder and  creating MPD file using MP4Box.

### What is this repository for? ###

* ffmpeg encoder is used to encode media files into different bit rates and resolutions. After encoding, in order to segment the media files and generate Media Presentation Description (MPD) MP4Box is used. 

* Version for ffmpeg v2.8.11 and for MP4Box v0.7.2-DEV-rev332 


### How do I get set up? ###

* Installing ffmpeg on Ubuntu system (v16.0.4.3)
 sudo apt-get install ffmpeg
 
 * Installing MP4Box on Ubuntu system (v16.0.4.3)
 sudo apt-get install gpac
 
 
 * Following commands are run to get encoded media files and then generate MPD files using those encoded files:
 
 * By executing the following command, three video files will be generated at different bitrate with different resolution i.e. (640x360 at 750 Kbps) ,(320x180 at 500 Kbps) , (160x90 at 250 Kbps)
 
ffmpeg -i bunny.avi -c:v libx264 -keyint_min 150 \
-g 150 -f mp4 -dash 1 \
-an -vf scale=160:190 -b:v 250k -dash 1 video_160x90_250k.mp4 \
-an -vf scale=320:180 -b:v 500k -dash 1 video_320x180_500k.mp4 \
-an -vf scale=640:360 -b:v 750k -dash 1 video_640x360_750k.mp4

 * Executing below command will give Audio file at 128 kbps bitrate.

ffmpeg -i bunny.avi -c:a aac -ac 2 -ab 128k -strict -2 -vn video_audio.mp4

* Executing below command will generate MPD file using the encoded media files

MP4Box -dash 1000 -rap -frag-rap -profile onDemand -out bunny.mpd video_160x90_250k.mp4 video_320x180_500k.mp4 video_640x360_750k.mp4 video_audio.mp4

### Where are the media files  ###

All media files along with MPD file are in the Downloads tab of this repository


### Who do I talk to? ###

* Mamoona Aslam